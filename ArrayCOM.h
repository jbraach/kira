//
//  ArrayCOM.h
//  LED_Array
//
//  Created by Justus Braach on 01.07.20.
//  Copyright © 2020 Justus Braach. All rights reserved.
//

#ifndef ArrayCOM_h
#define ArrayCOM_h

#include <stdio.h>
#include <stdlib.h>
#include <string> 
#include <unistd.h> //ssize_t

#include "SerialPort.h"

using namespace::std;

class ArrayCOM {
    
private:
    SerialPort* serialport;
    
public:
    ArrayCOM();
    ~ArrayCOM();
    
    void CloseCOM(void);
    void OpenCOM(void);
    void SetIntens(int channel, int intensity);
    void CalibrateIntens();
    void SetFreq(int frequency);
    void SetPW(int pulsewidth);
    void WorkingState(const char* input);
    
};

#endif /* ArrayCOM_h */
