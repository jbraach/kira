//
//  ArrayCOM.cc
//  LED_Array
//
//  Created by Justus Braach on 01.07.20.
//  Copyright © 2020 Justus Braach. All rights reserved.
//
#include "SerialPort.h"
#include "ArrayCOM.h"

using namespace std;

static const int numBytes = 256;

// Constructor
ArrayCOM::ArrayCOM() {
    serialport = new SerialPort();
}

// Destructor
ArrayCOM::~ArrayCOM() {
    delete serialport;
    serialport = nullptr;
}

// Open Communication
void ArrayCOM::OpenCOM(){
    // Open port and connect to a device
    const char devicePathStr[] = "/dev/cu.usbmodem14101";
    const int baudRate = 9600;
    serialport->connectDevice(devicePathStr, baudRate);
}

// Close Communication
void ArrayCOM::CloseCOM() {
    // Close port and disconnect from device
    serialport->closeSerialPort();
}

// Set the intensity of each channel
void ArrayCOM::SetIntens(int chan, int intens) {
    
    string prefix = "CH_";
    char *intenstemp = new char[9];
    for (int i = 0; i < prefix.length(); i++) {
        intenstemp[i] = prefix[i];
    }
    if (chan/10 < 1) {
        intenstemp[prefix.length()] = '0';
        intenstemp[prefix.length() + 1] = chan;
    }
    else {
        intenstemp[prefix.length()] = chan;
    }
    intenstemp[prefix.length() + 2] = '_';
    intenstemp[prefix.length() + 3] = intens;
    
    serialport->writeSerialData(intenstemp, numBytes);
}

// Calibrate the intensity of each channel
void ArrayCOM::CalibrateIntens() {
    
}

// Set the pulse frequency
void ArrayCOM::SetFreq(int freq) {
    
    string prefix = "FREQ_";
    char *freqtemp = new char[10];
    for (int i = 0; i < prefix.length() ; i++) {
        freqtemp[i] = prefix[i];
    }
    freqtemp[prefix.length()] = freq;
    
    serialport->writeSerialData(freqtemp, numBytes);
}

// Set the pulse width
void ArrayCOM::SetPW(int pulsew) {
        
    string prefix = "PW_";
    char *freqtemp = new char[10];
    for (int i = 0; i < prefix.length() ; i++) {
        freqtemp[i] = prefix[i];
    }
    freqtemp[prefix.length()] = pulsew;
    
    serialport->writeSerialData(freqtemp, numBytes);
}

// Start/Stop pulse output
void ArrayCOM::WorkingState(const char* input) {
    
    serialport->writeSerialData(input, numBytes);
}
