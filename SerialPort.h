//
//  SerialPort.h
//  SerialCOM
//
//  Created by Justus Braach on 19.06.20.
//  Copyright © 2020 Justus Braach. All rights reserved.
//

#ifndef __SERIAL_PORT_HPP__
#define __SERIAL_PORT_HPP__

#include <string>
#include <unistd.h> //ssize_t

using namespace::std;

class SerialPort{
    
private:

public:
    
    SerialPort();
    ~SerialPort();

    int openAndConfigureSerialPort(const char* portPath, int baudRate);
    int getSerialFileDescriptor(void);
    int connectDevice(const char* portPath, int baudRate);

    bool serialPortIsOpen();

    ssize_t flushSerialData();
    ssize_t writeSerialData(const char* bytes, size_t length);
    ssize_t readSerialData(char* bytes, size_t length);
    ssize_t closeSerialPort(void);

};

#endif //__SERIAL_PORT_HPP__
