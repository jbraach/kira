//
//  main.cpp
//  LED_Array
//
//  Created by Justus Braach on 01.07.20.
//  Copyright © 2020 Justus Braach. All rights reserved.
//

#include <iostream>

#include "ArrayCOM.h"

using namespace std;

int main(int argc, const char * argv[]) {
    
    ArrayCOM kira;
    
    kira.OpenCOM();
    string keyboard;
    
    while (keyboard != "exit") {
        cin >> keyboard;
        char *command = new char[keyboard.size()];
        copy(keyboard.begin(), keyboard.end(), command);
        //input[keyboard.size()] = '\n';
        
        kira.WorkingState(command);
    
        // Write serial data
    
        // Read serial data
        
        // * Remember to flush potentially buffered data when necessary
        
        // Close serial port when done
    
    }
    
    return 0;
}
